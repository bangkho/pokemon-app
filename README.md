Pokemon App - Ahmad Khoirudin
=============================
This is a simple web app that displays a list of Pokemon and do little games about Pokemon evolution. This app is built using Next.js, Tailwind CSS, and the Pokemon API.

## How to run the app

First, you need to install the dependencies:

```bash
yarn install
```

Then, you can run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to use the app.

You can visit the deployed app on [Pokemon App - Ahmad Khoirudin](https://pokemon-app-delta-two.vercel.app/).
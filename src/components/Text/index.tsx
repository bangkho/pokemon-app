"use client";

import React from 'react';

interface TextProps {
  children: string | string[];
  className?: string;
}

const Text: React.FC<TextProps> = ({ children, className }) => {
  return <p className={`text-base ${className}`}>{children}</p>;
};

Text.defaultProps = {
  children: 'Default Text',
};

export default Text;
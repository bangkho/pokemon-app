import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from './index';

describe('Button', () => {
  it('renders the button correctly', () => {
    const onClick = jest.fn();
    const { getByText } = render(
      <Button onClick={onClick} className="custom-button">
        Click me
      </Button>
    );

    const button = getByText('Click me');
    expect(button).toBeInTheDocument();
    expect(button).toHaveClass('custom-button');
  });

  it('calls the onClick function when clicked', () => {
    const onClick = jest.fn();
    const { getByText } = render(
      <Button onClick={onClick}>
        Click me
      </Button>
    );

    const button = getByText('Click me');
    fireEvent.click(button);
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
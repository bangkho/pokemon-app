"use client";
import { useState } from 'react';
import useSound from 'use-sound';
import { SpeakerWaveIcon, SpeakerXMarkIcon } from '@heroicons/react/24/solid'

const SoundPlay = () => {
  const [play, { pause, sound }] = useSound('/furret_walk.mp3', { volume: 0.2, loop: true, autoplay: true });
  const [playing, setPlaying] = useState(sound?.playing() || true)

  const handlePlay = () => {
    console.log(sound?.playing());
    if(playing) {
      pause();
      setPlaying(false);
    } else {
      play();
      setPlaying(true);
    }
  }

  return (
    <button className='sticky bottom-40 left-[80vw] m-auto p-3 bg-white rounded-full shadow-md' onClick={handlePlay}>
      {playing ? <SpeakerWaveIcon className="h-6 w-6 text-dark-theme-pokemon"/> : <SpeakerXMarkIcon className="h-6 w-6 text-dark-theme-pokemon"/>}
    </button>
  );
};

export default SoundPlay;
import { render, screen, fireEvent } from '@testing-library/react';
import SoundPlay from './index';

describe('SoundPlay', () => {
  it('should render the SoundPlay component', () => {
    render(<SoundPlay />);
    const soundPlayButton = screen.getByRole('button');
    expect(soundPlayButton).toBeInTheDocument();
  });

  it('should toggle the sound when the button is clicked', () => {
    render(<SoundPlay />);
    const soundPlayButton = screen.getByRole('button');
    const speakerWaveIcon = screen.getByTestId('speaker-wave-icon');
    const speakerXMarkIcon = screen.getByTestId('speaker-xmark-icon');

    // Initial state: sound is playing
    expect(speakerWaveIcon).toBeInTheDocument();
    expect(speakerXMarkIcon).not.toBeInTheDocument();

    // Click the button to pause the sound
    fireEvent.click(soundPlayButton);

    expect(speakerWaveIcon).not.toBeInTheDocument();
    expect(speakerXMarkIcon).toBeInTheDocument();

    // Click the button again to play the sound
    fireEvent.click(soundPlayButton);

    expect(speakerWaveIcon).toBeInTheDocument();
    expect(speakerXMarkIcon).not.toBeInTheDocument();
  });
});
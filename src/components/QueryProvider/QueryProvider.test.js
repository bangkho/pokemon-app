import { render } from '@testing-library/react';
import { QueryClientProvider } from '@tanstack/react-query';
import QueryProvider from './QueryProvider';

describe('QueryProvider', () => {
  test('renders children', () => {
    const { getByText } = render(
      <QueryClientProvider>
        <QueryProvider>
          <div>Test Children</div>
        </QueryProvider>
      </QueryClientProvider>
    );

    expect(getByText('Test Children')).toBeInTheDocument();
  });

  test('renders ReactQueryDevtools', () => {
    const { getByText } = render(
      <QueryClientProvider>
        <QueryProvider>
          <div>Test Children</div>
        </QueryProvider>
      </QueryClientProvider>
    );

    expect(getByText('React Query Devtools')).toBeInTheDocument();
  });
});
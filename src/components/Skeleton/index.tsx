import React from 'react';

interface SkeletonProps {
  width?: string;
  height?: string;
  borderRadius?: string;
}

const Skeleton: React.FC<SkeletonProps> = ({ width = '100%', height = '100%', borderRadius = 'rounded-md' }) => {
  return (
    <div className={`animate-pulse bg-gray-300 ${borderRadius}`} style={{ width, height }}>
      &nbsp;
    </div>
  );
};

export default Skeleton;
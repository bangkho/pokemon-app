import axiosAdapter from '../fetch';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

jest.mock('axios');

describe('axiosAdapter', () => {
  const mockConfig = {
    url: '/example',
    method: 'GET',
  };

  const mockResponse = {
    data: {},
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
  };

  it('should return the response when the request is successful', async () => {
    axios.request.mockResolvedValueOnce(mockResponse);

    const response = await axiosAdapter(mockConfig);

    expect(response).toEqual(mockResponse);
    expect(axios.request).toHaveBeenCalledWith(mockConfig);
  });

  it('should throw an error when the request fails', async () => {
    const mockError = new Error('Request failed');
    axios.request.mockRejectedValueOnce(mockError);

    await expect(axiosAdapter(mockConfig)).rejects.toThrow(mockError);
    expect(axios.request).toHaveBeenCalledWith(mockConfig);
  });
});
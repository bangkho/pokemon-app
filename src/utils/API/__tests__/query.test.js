import { renderHook } from '@testing-library/react-hooks';
import { usePokemonQuery } from './query';

describe('usePokemonQuery', () => {
  test('should fetch data successfully', async () => {
    const mockData = { name: 'Pikachu', type: 'Electric' };
    const mockEndpoint = 'https://api.example.com/pokemon';
    const mockOptions = { retry: 3 };

    // Mock axiosAdapter function
    jest.mock('@/utils/API/fetch', () => ({
      __esModule: true,
      default: jest.fn().mockResolvedValue({ data: mockData }),
    }));

    const { result, waitForNextUpdate } = renderHook(() =>
      usePokemonQuery('pokemon', mockEndpoint, mockOptions)
    );

    expect(result.current.isLoading).toBe(true);

    await waitForNextUpdate();

    expect(result.current.isLoading).toBe(false);
    expect(result.current.data).toEqual(mockData);
  });
});
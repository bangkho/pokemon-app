import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

const api: AxiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_HOST,
  timeout: 5000,
});

const axiosAdapter = async (config: AxiosRequestConfig): Promise<AxiosResponse> => {

  return await api.request(config)
    .then(response => response)
    .catch(error => {
      console.error(error);
      throw error;
    });
};

export default axiosAdapter;

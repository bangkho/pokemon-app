import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import axiosAdapter from '@/utils/API/fetch';
import { PokemonList } from '@/modules/pokemon/domain/model/PokemonList';

const usePokemonQuery = (queryKey: string, endpoint: string, options: any) => {
  const query = useQuery({
    ...options,
    queryKey: [queryKey],
    queryFn: () => axiosAdapter({ url: endpoint })
      .then(res => res.data),
    })
  return query;
}

const usePokemonInfiniteQuery = (queryKey: string, endpoint: string, options: any) => {
  const query = useInfiniteQuery({
    ...options,
    queryKey: [queryKey],
    initialPageParam: 0,
    getNextPageParam: (lastPage: any[], allPages: any[]) => {
      const nextPage = lastPage.length === 42 ? allPages.length * 42 : undefined;
      return nextPage;
    },
    queryFn: () => axiosAdapter({ url: endpoint })
      .then(res => res.data),
    })
  return query;
}

export { usePokemonQuery, usePokemonInfiniteQuery };

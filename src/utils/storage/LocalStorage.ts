"use client"
let storage: any;

if(typeof window !== 'undefined') {
  storage = window.localStorage
}

export function getItem(key: string): string | null {
  return storage.getItem(key);
}

export function setItem(key: string, value: string): void {
  storage.setItem(key, value);
}

export function removeItem(key: string): void {
  storage.removeItem(key);
}

export function clear(): void {
  storage.clear();
}

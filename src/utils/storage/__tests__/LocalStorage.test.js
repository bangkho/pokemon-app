import { getItem, setItem, removeItem, clear } from '../LocalStorage';

describe('LocalStorage', () => {
  beforeEach(() => {
    // Mock the localStorage object
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(),
        setItem: jest.fn(),
        removeItem: jest.fn(),
        clear: jest.fn(),
      },
      writable: true,
    });
  });

  afterEach(() => {
    // Clear the mock implementation after each test
    jest.clearAllMocks();
  });

  it('should call localStorage.getItem with the provided key', () => {
    const key = 'testKey';
    getItem(key);
    expect(window.localStorage.getItem).toHaveBeenCalledWith(key);
  });

  it('should call localStorage.setItem with the provided key and value', () => {
    const key = 'testKey';
    const value = 'testValue';
    setItem(key, value);
    expect(window.localStorage.setItem).toHaveBeenCalledWith(key, value);
  });

  it('should call localStorage.removeItem with the provided key', () => {
    const key = 'testKey';
    removeItem(key);
    expect(window.localStorage.removeItem).toHaveBeenCalledWith(key);
  });

  it('should call localStorage.clear', () => {
    clear();
    expect(window.localStorage.clear).toHaveBeenCalled();
  });
});

export const BERRY_CONSTANTS = {
  'very-soft': 2,
  'soft': 3,
  'hard': 5,
  'very-hard': 8,
  'super-hard': 10,
}


import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import QueryProvider from "@/components/QueryProvider/QueryProvider";
import SoundPlay from "@/components/SoundPlay";
import Image from "next/image";
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Pokemon App - Ahmad Khoirudin",
  description: "Gotta Catch 'Em All",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <QueryProvider>
          <div className="flex flex-col w-full p-6 items-center">
            <Link href="/" className="cursor-pointer">
              <Image className='pb-6' src="/assets/pokemon_logo.png" alt="Pokemon Logo" width={400} height={200}/>
            </Link>
            {children}
            <SoundPlay/>
          </div>
        </QueryProvider>
      </body>
    </html>
  );
}

"use client"

import Button from '@/components/Button'
import Text from '@/components/Text'
import useBerry from '@/modules/berry/domain/use-case/Berry'
import BerryList from '@/modules/berry/presentation/BerryList'
import usePokemonSpecies from '@/modules/evolution/domain/use-case/UsePokemonSpecies'
import PokemonDetail from '@/modules/pokemon/data/PokemonDetail'
import usePokemonFeed from '@/modules/pokemon/domain/use-case/UsePokemonFeed'
import PokemonStatus from '@/modules/pokemon/presentation/PokemonStatus'
import { setItem } from '@/utils/storage/LocalStorage'
import Link from 'next/link'
import { useParams } from 'next/navigation'
import React, { useEffect } from 'react'

function Pokemon() {
  const { name } = useParams()
  const { data, isLoading, isError } = PokemonDetail(name as string)
  const { addPokemonWeight, initPokemonWeight, pokemonWeight } = usePokemonFeed()
  const { handleSelectBerry, selectedBerry, berryDetails, handleCalculate } = useBerry()

  const { handleSelectPokemon, pokemonEvolveTo } = usePokemonSpecies()

  useEffect(() => {
    handleSelectPokemon(name as string)
  }, [name, handleSelectPokemon])

  useEffect(() => {
    if(isError) {
      alert('Pokemon not found')
    } else if(data){
      setItem('pokemon', JSON.stringify(data))
      initPokemonWeight(data.weight)
    }
  }, [isError, data])

  return (
    <>
      <PokemonStatus pokemon={data} isLoading={isLoading} pokemonWeight={pokemonWeight}/>
      <BerryList selectedBerry={selectedBerry} handleSelectBerry={handleSelectBerry}/>
      {selectedBerry && <Text className='text-xl font-bold text-dark-theme-pokemon px-6 py-2 my-2 rounded-lg bg-accent-light-theme-pokemon capitalize'>{`${selectedBerry} Berry - ${berryDetails?.firmness.name ?? '???'}`}</Text>}
      {pokemonWeight >= (pokemonEvolveTo?.weight || 0) ?
        <Link href={`/pokemon/${pokemonEvolveTo?.name}`}><Text className='text-xl font-bold text-dark-theme-pokemon px-6 py-2 my-2 rounded-lg bg-accent-light-theme-pokemon capitalize'>{`Evolve to ${pokemonEvolveTo?.name ?? '???'}`}</Text></Link>
        : <Button className='m-4 py-2 px-6 rounded-lg w-full sticky font-bold bg-dark-theme-pokemon text-white bottom-7 md:w-72' onClick={() => addPokemonWeight(handleCalculate)}>Feed Me!</Button>}
    </>
  )
}

export default Pokemon

import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { useRouter } from 'next/router'
import React from 'react'
import Pokemon from '../page'

jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}))

jest.mock('/src/modules/berry/domain/use-case/Berry.ts', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    handleSelectBerry: jest.fn(),
    selectedBerry: 'Oran',
    berryDetails: { firmness: { name: 'Soft' } },
    handleCalculate: jest.fn(),
  })),
}))

jest.mock('/src/modules/evolution/domain/use-case/UsePokemonSpecies.ts', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    handleSelectPokemon: jest.fn(),
    pokemonEvolveTo: { name: 'Charizard', weight: 905 },
  })),
}))

jest.mock('/src/modules/pokemon/data/PokemonDetail.ts', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    data: { name: 'Charmander', weight: 85 },
    isLoading: false,
    isError: false,
  })),
}))

jest.mock('/src/modules/pokemon/domain/use-case/UsePokemonFeed.ts', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    addPokemonWeight: jest.fn(),
    initPokemonWeight: jest.fn(),
    pokemonWeight: 100,
  })),
}))

jest.mock('/src/utils/storage/LocalStorage.ts', () => ({
  setItem: jest.fn(),
}))

describe('Pokemon', () => {
  beforeEach(() => {
    useRouter.mockReturnValue({
      query: { name: 'charmander' },
    })
  })

  it('renders the Pokemon component', () => {
    render(<Pokemon />)
    expect(screen.getByText('Charmander')).toBeInTheDocument()
  })

  it('renders the selected berry and its details', () => {
    render(<Pokemon />)
    expect(screen.getByText('Oran Berry - Soft')).toBeInTheDocument()
  })

  it('renders the "Evolve to Charizard" button', () => {
    render(<Pokemon />)
    expect(screen.getByText('Evolve to Charizard')).toBeInTheDocument()
  })

  it('renders the "Feed Me!" button', () => {
    render(<Pokemon />)
    expect(screen.getByText('Feed Me!')).toBeInTheDocument()
  })

  it('calls the addPokemonWeight function when "Feed Me!" button is clicked', () => {
    const addPokemonWeight = jest.fn()
    const handleCalculate = jest.fn()
    jest.mock('/src/modules/pokemon/domain/use-case/UsePokemonFeed.ts', () => ({
      __esModule: true,
      default: jest.fn(() => ({
        addPokemonWeight,
        initPokemonWeight: jest.fn(),
        pokemonWeight: 100,
      })),
    }))
    jest.mock('/src/modules/berry/domain/use-case/Berry.ts', () => ({
      __esModule: true,
      default: jest.fn(() => ({
        handleSelectBerry: jest.fn(),
        selectedBerry: null,
        berryDetails: null,
        handleCalculate,
      })),
    }))
    render(<Pokemon />)
    userEvent.click(screen.getByText('Feed Me!'))
    expect(addPokemonWeight).toHaveBeenCalled()
    expect(handleCalculate).toHaveBeenCalled()
  })
})
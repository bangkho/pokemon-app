"use client"
import PokemonList from '@/modules/pokemon/presentation/PokemonList';
import Text from '@/components/Text';

export default function Home() {

  return (
    <>
      <Text className='text-xl font-bold text-dark-theme-pokemon m-4'>Choose Your Pokemon!</Text>
      <PokemonList/>
    </>
  );
}

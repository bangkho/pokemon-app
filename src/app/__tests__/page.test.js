import { render, screen } from '@testing-library/react';
import Home from '../page';

describe('Home', () => {
  test('renders the heading text', () => {
    render(<Home />);
    const headingText = screen.getByText('Choose Your Pokemon!');
    expect(headingText).toBeInTheDocument();
  });

  test('renders the PokemonList component', () => {
    render(<Home />);
    const pokemonList = screen.getByTestId('pokemon-list');
    expect(pokemonList).toBeInTheDocument();
  });
});
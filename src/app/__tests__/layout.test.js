import { render } from "@testing-library/react";
import RootLayout from "../layout";

describe("RootLayout", () => {
  it("renders children correctly", () => {
    const { getByText } = render(
      <RootLayout>
        <div>Test Child</div>
      </RootLayout>
    );

    expect(getByText("Test Child")).toBeInTheDocument();
  });

  it("renders the Pokemon logo", () => {
    const { getByAltText } = render(<RootLayout />);

    expect(getByAltText("Pokemon Logo")).toBeInTheDocument();
  });

  // Add more tests as needed
});
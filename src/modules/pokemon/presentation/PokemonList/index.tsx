"use client"

import { Pokemon } from "@/modules/pokemon/domain/model/PokemonList"
import PokemonListFetch from "@/modules/pokemon/data/PokemonList"
import PokemonCard from "@/modules/pokemon/presentation/PokemonCard"
import usePokemon from "../../domain/use-case/UsePokemon"
import Button from "@/components/Button"
import SearchBar from "../components/SearchBar"
import { useEffect } from "react"
import { useInView } from "react-intersection-observer"

function PokemonList() {
  const {ref, inView} = useInView();
  const { handleSelectPokemon, selectedPokemon, handleSubmit, parsePokemonImage, search, setSearch } = usePokemon();
  const { data, hasNextPage, fetchNextPage } = PokemonListFetch()

  useEffect(() => {
    if (search) {
      const timer = setTimeout(() => {
        console.log('searching', search)
      }, 500)
      return () => clearTimeout(timer)
    }
  }, [search])

  useEffect(() => {
    if (search) {
      fetchNextPage();
    }
  }, [search, inView, hasNextPage, fetchNextPage])

  return (
    <>
      <SearchBar value={search} setSearch={setSearch} />
      <div className="grid grid-cols-3 gap-2 md:grid-cols-4 lg:grid-cols-6 overflow-auto min-h-screen">
        {data?.pages?.map((page: any) =>
          page?.results?.map((pokemon: Pokemon, index: number) => {
            if (page.length == index + 1){
              return (
                <div ref={ref} key={pokemon.name}>
                  <PokemonCard
                  pokemon={pokemon}
                  handleSelectPokemon={handleSelectPokemon}
                  parsePokemonImage={parsePokemonImage}
                  selectedPokemon={selectedPokemon}
                />
                </div>
              )
            } else {
              return (
                <div key={pokemon.name}>
                  <PokemonCard
                    pokemon={pokemon}
                    handleSelectPokemon={handleSelectPokemon}
                    parsePokemonImage={parsePokemonImage}
                    selectedPokemon={selectedPokemon}
                  />
                </div>
              )
            }
          })
        )}
      </div>
      <Button
        className="m-4 py-2 px-6 rounded-lg w-full sticky font-bold bg-dark-theme-pokemon text-white bottom-10 md:w-72"
        onClick={() => handleSubmit()}
      >
        I Choose You!
      </Button>
    </>
  );
}

export default PokemonList


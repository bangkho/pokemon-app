"use client"
import { Pokemon } from "@/modules/pokemon/domain/model/PokemonList"
import Image from "next/image";
import Text from "@/components/Text";

const PokemonCard = ({ pokemon, handleSelectPokemon, selectedPokemon, parsePokemonImage }:
    { pokemon: Pokemon; handleSelectPokemon: (name: string) => void; selectedPokemon: string, parsePokemonImage: (url: string) => string; }) => {

  return (
    <div className={`flex center flex-col ${selectedPokemon === pokemon.name ? 'bg-accent-light-theme-pokemon' : 'bg-accent-theme-pokemon'} rounded-lg cursor-pointer hover:bg-accent-light-theme-pokemon shadow-md`} onClick={() => handleSelectPokemon(pokemon.name)} >
      <Image className="self-center" width={100} height={100} src={parsePokemonImage(pokemon.url)} alt={pokemon.name} />
      <Text className={`self-center capitalize align-middle text-dark-theme-pokemon p-2
        ${selectedPokemon === pokemon.name ?  'font-bold' : 'font-normal'}`}>{pokemon.name}</Text>
    </div>
  )
}

export default PokemonCard;

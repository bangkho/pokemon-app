import { render, screen, fireEvent } from '@testing-library/react';
import PokemonCard from './index';

describe('PokemonCard', () => {
  const pokemon = {
    name: 'Pikachu',
    // Add other properties as needed
  };

  const handleSelectPokemon = jest.fn();
  const selectedPokemon = '';

  it('renders the PokemonCard component', () => {
    render(
      <PokemonCard
        pokemon={pokemon}
        handleSelectPokemon={handleSelectPokemon}
        selectedPokemon={selectedPokemon}
      />
    );

    // Assert that the component renders without errors
    expect(screen.getByText(pokemon.name)).toBeInTheDocument();
  });

  it('calls handleSelectPokemon when clicked', () => {
    render(
      <PokemonCard
        pokemon={pokemon}
        handleSelectPokemon={handleSelectPokemon}
        selectedPokemon={selectedPokemon}
      />
    );

    // Simulate a click event on the component
    fireEvent.click(screen.getByText(pokemon.name));

    // Assert that handleSelectPokemon is called with the correct arguments
    expect(handleSelectPokemon).toHaveBeenCalledWith(pokemon.name);
  });
});
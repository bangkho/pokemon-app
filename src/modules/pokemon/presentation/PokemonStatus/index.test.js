import { render, screen, fireEvent } from '@testing-library/react';
import PokemonStatus from './index';

describe('PokemonStatus', () => {
  const pokemon = {
    name: 'Pikachu',
    sprites: {
      other: {
        'official-artwork': {
          front_default: 'https://example.com/pikachu.png',
        },
      },
    },
    stats: [
      { base_stat: 80 },
      { base_stat: 70 },
      { base_stat: 60 },
      { base_stat: 90 },
    ],
  };
  const isLoading = false;
  const pokemonWeight = 10;

  test('renders the Pokemon name', () => {
    render(<PokemonStatus pokemon={pokemon} isLoading={isLoading} pokemonWeight={pokemonWeight} />);
    const pokemonNameElement = screen.getByText('Pikachu');
    expect(pokemonNameElement).toBeInTheDocument();
  });

  test('renders the delete button', () => {
    const handleDeletePokemonMock = jest.fn();
    jest.mock('../../domain/use-case/UseChoosePokemon', () => ({
      __esModule: true,
      default: () => ({
        handleDeletePokemon: handleDeletePokemonMock,
      }),
    }));

    render(<PokemonStatus pokemon={pokemon} isLoading={isLoading} pokemonWeight={pokemonWeight} />);
    const deleteButton = screen.getByRole('button');
    fireEvent.click(deleteButton);
    expect(handleDeletePokemonMock).toHaveBeenCalled();
  });

  test('renders the Pokemon image', () => {
    render(<PokemonStatus pokemon={pokemon} isLoading={isLoading} pokemonWeight={pokemonWeight} />);
    const pokemonImage = screen.getByAltText('Pikachu');
    expect(pokemonImage).toBeInTheDocument();
    expect(pokemonImage).toHaveAttribute('src', 'https://example.com/pikachu.png');
  });

  test('renders the Pokemon stats', () => {
    render(<PokemonStatus pokemon={pokemon} isLoading={isLoading} pokemonWeight={pokemonWeight} />);
    const hpStat = screen.getByText('HP: 80');
    const attackStat = screen.getByText('Attack: 70');
    const defenseStat = screen.getByText('Defense: 60');
    const speedStat = screen.getByText('Speed: 90');

    expect(hpStat).toBeInTheDocument();
    expect(attackStat).toBeInTheDocument();
    expect(defenseStat).toBeInTheDocument();
    expect(speedStat).toBeInTheDocument();
  });

  test('renders the Pokemon weight', () => {
    render(<PokemonStatus pokemon={pokemon} isLoading={isLoading} pokemonWeight={pokemonWeight} />);
    const weightElement = screen.getByText('Weight: 10 KG');
    expect(weightElement).toBeInTheDocument();
  });
});
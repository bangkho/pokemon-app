import React from 'react'
import Text from '@/components/Text'
import { XCircleIcon } from '@heroicons/react/24/solid'
import Image from 'next/image'
import Pokemon from '@/modules/pokemon/domain/model/Pokemon'
import usePokemonFeed from '../../domain/use-case/UsePokemonFeed'
import useChoosePokemon from '../../domain/use-case/UsePokemon'

function PokemonStatus ({ pokemon, isLoading, pokemonWeight }: { pokemon: Pokemon, isLoading: boolean, pokemonWeight: number}) {
  const { handleDeletePokemon } = useChoosePokemon()

  return (
    <>
      <Text className='text-xl font-bold text-dark-theme-pokemon px-6 py-2 mb-2 rounded-lg bg-accent-light-theme-pokemon capitalize'>{pokemon?.name}</Text>
      <div onClick={() => handleDeletePokemon()}>
        <XCircleIcon className="h-10 w-10 ml-64 self-end rounded-full text-dark-theme-pokemon cursor-pointer"/>
      </div>
      {isLoading ? [] : <Image src={pokemon?.sprites.other["official-artwork"].front_default} alt={pokemon?.name} width={200} height={200}/>}
      <div className='grid grid-cols-2 gap-4'>
        <Text className='text-xl font-bold text-dark-theme-pokemon py-1'>{`HP: ${pokemon?.stats[0].base_stat}`}</Text>
        <Text className='text-xl font-bold text-dark-theme-pokemon py-1'>{`Attack: ${pokemon?.stats[1].base_stat}`}</Text>
        <Text className='text-xl font-bold text-dark-theme-pokemon py-1'>{`Defense: ${pokemon?.stats[2].base_stat}`}</Text>
        <Text className='text-xl font-bold text-dark-theme-pokemon py-1'>{`Speed: ${pokemon?.stats[3].base_stat}`}</Text>
      </div>
      <Text className='text-xl font-bold text-dark-theme-pokemon px-6 py-4'>{`Weight: ${pokemonWeight} KG`}</Text>
    </>
  );
};

export default PokemonStatus;
import { MagnifyingGlassIcon } from '@heroicons/react/24/outline';

interface SearchBarProps {
  value: string;
  setSearch: (value: string) => void;
}

const SearchBar = ({ value, setSearch }: SearchBarProps) => {
  return (
    <div className="flex items-center w-1/2 lg:1/4 bg-gray-200 rounded-md p-2 mb-4">
      <MagnifyingGlassIcon className="h-5 w-5 text-gray-500" />
      <input
        type="text"
        placeholder="Search Pokemon..."
        className="ml-2 bg-transparent outline-none w-full"
        onChange={(e) => setSearch(e.target.value)}
        value={value}
      />
    </div>
  );
};

export default SearchBar;
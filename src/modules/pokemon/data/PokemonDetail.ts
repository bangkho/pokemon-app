"use client"

import { usePokemonQuery } from "@/utils/API/query"

import Pokemon from "@/modules/pokemon/domain/model/Pokemon";

function PokemonDetail(id: string, enabled: boolean = true) {
  const { data, isLoading, isError, refetch } = usePokemonQuery(id, `pokemon/${id}`, { staleTime: Infinity, enabled});
  return { data: data as Pokemon, isLoading, isError, refetch};
}

export default PokemonDetail;

"use client"

import { usePokemonInfiniteQuery } from "@/utils/API/query"

function PokemonList(offset: number = 0) {
  const {data, isLoading, hasNextPage, fetchNextPage} = usePokemonInfiniteQuery('pokemonList', `pokemon/?offset=${offset}&limit=252`, { staleTime: Infinity})

  return { data , isLoading, hasNextPage, fetchNextPage}
}

export default PokemonList

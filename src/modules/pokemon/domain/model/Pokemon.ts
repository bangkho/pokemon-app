interface Stats {
  base_stat: number
}

interface Pokemon {
  id: number;
  name: string;
  types: string[];
  abilities: string[];
  height: number;
  weight: number;
  base_experience: number;
  stats: Stats[];
  sprites: {
    front_default: string;
    front_shiny: string;
    back_default: string;
    back_shiny: string;
    other: {
      "official-artwork": {
        front_default: string;
        front_shy: string;
      }
    }
  };
}

export default Pokemon;
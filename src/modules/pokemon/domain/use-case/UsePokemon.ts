"use client"
import { removeItem, setItem } from '@/utils/storage/LocalStorage';
import { useState } from 'react';
import { useRouter } from 'next/navigation'

function usePokemon() {
  const [selectedPokemon, setSelectedPokemon] = useState<string>('');
  const [search, setSearch] = useState<string>('');

  const router = useRouter();

  const handleSelectPokemon = (pokemon: string) => {
    setSelectedPokemon(pokemon);
  };

  const parsePokemonImage = (url: string) => {
    const urlParts = url.split('/');
    const pokemonDefaultImage = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/';

    return pokemonDefaultImage + urlParts[urlParts.length - 2] + '.png';
  }

  const handleSubmit = () => {
    setItem('pokemonName', selectedPokemon);
    router.push(`/pokemon/${selectedPokemon}`);
  };

  const handleDeletePokemon = () => {
    removeItem('pokemonName');
    removeItem('pokemonWeight');
    removeItem('pokemon');
    router.push('/');
  };

  return {
    selectedPokemon,
    handleSelectPokemon,
    handleSubmit,
    handleDeletePokemon,
    parsePokemonImage,
    search,
    setSearch,
  };
}

export default usePokemon;
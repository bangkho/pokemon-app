import { setItem } from '@/utils/storage/LocalStorage';
import { useState, useEffect } from 'react';

function usePokemonFeed() {
  const [pokemonWeight, setPokemonWeight] = useState<number>(0);

  useEffect(() => {
    const storedWeight = localStorage.getItem('pokemonWeight');
    if (storedWeight) {
      setPokemonWeight(Number(storedWeight));
    }
  }, []);

  const initPokemonWeight = (weight: number) => {
    setPokemonWeight(weight);
    setItem('pokemonWeight', weight.toString());
  };

  const addPokemonWeight = (calculateWeight: () => number) => {
    const weight = calculateWeight()
    if(weight+pokemonWeight <= 0){
      setPokemonWeight(1);
      setItem('pokemonWeight', (1).toString());
    } else {
      setPokemonWeight(pokemonWeight + weight);
      setItem('pokemonWeight', (pokemonWeight + weight).toString());
    }
  };

  return { pokemonWeight, initPokemonWeight, addPokemonWeight };
}

export default usePokemonFeed;
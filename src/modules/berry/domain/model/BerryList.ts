interface BerryList {
  count: number;
  next: string | null;
  previous: string | null;
  results: Berry[];
}

interface Berry {
  name: string;
  url: string;
}

export type { Berry, BerryList };
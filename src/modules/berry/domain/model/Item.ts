interface Item {
  id: number;
  name: string;
  cost: number;
  attributes: string[];
  category: string;
  effect: string;
  flavorText: string;
  sprites: {
    default: string;
  };
}

export default Item;
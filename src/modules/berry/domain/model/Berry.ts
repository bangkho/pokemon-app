interface Berry {
  id: number;
  name: string;
  growth_time: number;
  max_harvest: number;
  natural_gift_power: number;
  size: number;
  smoothness: number;
  soil_dryness: number;
  firmness: BerryFirmness;
  flavors: BerryFlavor[];
}

interface BerryFlavor {
  flavor: string;
  potency: number;
}

interface BerryFirmness {
  name: string;
}

export default Berry;
import { useState, useEffect } from 'react';
import BerryDetail from '@/modules/berry/data/BerryDetail';
import Berry from '../model/Berry';
import { getItem, setItem } from '@/utils/storage/LocalStorage';
import { BERRY_CONSTANTS } from '@/utils/constans/Berry';



const useBerry = () => {
  const [selectedBerry, setSelectedBerry] = useState<string>('');
  const [berryDetails, setBerryDetails] = useState<Berry | null>(null);
  const { data, refetch } = BerryDetail(selectedBerry, !!selectedBerry);

  useEffect(() => {
    if (selectedBerry && data) {
      setBerryDetails(data);
    }
  }, [selectedBerry, data]);

  const handleSelectBerry =  (berry: string) => {
    setSelectedBerry(berry);
    refetch();
  };

  const handleCalculate = () => {
    const weight = checkWeightBerry(berryDetails)
    setItem('berry', JSON.stringify(berryDetails))
    return weight
  }

  const checkWeightBerry = (berry: Berry | null) => {
    let weight = 1;
    const berryFirmness = berry?.firmness.name

    const storageBerry = getItem('berry')
    if(storageBerry){
      const prevBerry: Berry = JSON.parse(storageBerry);
      if(prevBerry.firmness.name === berryFirmness){
        weight = -(BERRY_CONSTANTS[berryFirmness as keyof typeof BERRY_CONSTANTS] * 2)
      } else {
        weight = BERRY_CONSTANTS[berryFirmness as keyof typeof BERRY_CONSTANTS]
      }
    } else {
      weight = BERRY_CONSTANTS[berryFirmness as keyof typeof BERRY_CONSTANTS]
    }
    return weight
  }

  return { selectedBerry, berryDetails, handleSelectBerry, checkWeightBerry, handleCalculate };
};

export default useBerry;
"use client"

import BerryListFetch from "@/modules/berry/data/BerryList"
import { Berry } from "../../domain/model/BerryList"
import BerryCard from "../BerryCard"
import Text from "@/components/Text"

function BerryList({ selectedBerry, handleSelectBerry} : { selectedBerry: string, handleSelectBerry: (berry: string) => void}) {
  const { data } = BerryListFetch()

  return (
    <>
      <div className="flex flex-row items-start space-x-2 overflow-x-auto w-full mx-2 py-4">
        {data?.results.map((berry: Berry) => (
          <BerryCard berry={berry} key={berry.name} selectedBerry={selectedBerry} handleSelectBerry={handleSelectBerry}/>
        ))}
      </div>
    </>
  )
}

export default BerryList


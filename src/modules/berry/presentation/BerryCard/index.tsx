
import Image from "next/image";
import { Berry } from "@/modules/berry/domain/model/BerryList";
import ItemDetail from "@/modules/berry/data/ItemDetail";

const BerryCard = ({ berry, handleSelectBerry, selectedBerry }: { berry: Berry, handleSelectBerry: (name: string) => void, selectedBerry: string }) => {
  const { data, isLoading } = ItemDetail(berry.name)

  return (
      <div className={`flex-shrink-0 ${selectedBerry === berry.name ? `bg-accent-light-theme-pokemon` : `bg-accent-theme-pokemon`} rounded-full cursor-pointer hover:bg-accent-light-theme-pokemon`} onClick={() => handleSelectBerry(berry.name)}>
        {!isLoading && <Image className="p-1" width={50} height={50} src={data?.sprites.default} alt={data?.name} />}
      </div>
  )
}

export default BerryCard;
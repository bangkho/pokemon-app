"use client"

import { usePokemonQuery } from "@/utils/API/query"
import { BerryList as IBerryList } from "../domain/model/BerryList"

function BerryList() {
  const { data, isLoading } = usePokemonQuery('berryList', 'berry/?limit=64', { staleTime: Infinity})
  return { data: data as IBerryList, isLoading}
}

export default BerryList

"use client"

import { usePokemonQuery } from "@/utils/API/query"

import Berry from "@/modules/berry/domain/model/Berry";

function BerryDetail(id: string, enabled: boolean = false) {
  const { data, isLoading, isError, refetch } = usePokemonQuery(id, `berry/${id}`, { staleTime: Infinity, enabled});
  return { data: data as Berry, isLoading, isError, refetch };
}

export default BerryDetail;
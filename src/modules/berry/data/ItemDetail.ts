"use client"

import { usePokemonQuery } from "@/utils/API/query"

import Item from "@/modules/berry/domain/model/Item";

function ItemDetail(id: string) {
  const { data, isLoading } = usePokemonQuery(`${id}-item`, `item/${id}-berry`, { staleTime: Infinity});
  return { data: data as Item, isLoading};
}

export default ItemDetail;
interface EvolutionChain {
  id: number;
  babyTriggerItem: string | null;
  chain: ChainLink;
}

interface ChainLink {
  species: Species;
  evolves_to: ChainLink[];
}

interface Species {
  name: string;
  url: string;
}

export default EvolutionChain
"use client"

import { useState, useEffect } from 'react';
import { getItem, setItem } from '@/utils/storage/LocalStorage';
import EvolutionChain from '../../data/EvolutionChain';
import PokemonDetail from '@/modules/pokemon/data/PokemonDetail';

const useEvolution = () => {
  const [selectedPokemon, setSelectedPokemon] = useState<string>('');
  const [selectedEvolutionChain, setSelectedEvolutionChain] = useState<string>('');
  const { data: evolutionChainData, refetch: evolutionChainRefetch } = EvolutionChain(selectedEvolutionChain, !!selectedEvolutionChain);
  const { data: pokemonData, refetch: pokemonRefetch } = PokemonDetail(selectedPokemon, !!selectedPokemon);

  const handleSelectChain = (chain: string) => {
    setSelectedEvolutionChain(chain);
    evolutionChainRefetch();
  };

  const handleSelectEvolution = (selectedPokemon: string) => {
    setSelectedPokemon(selectedPokemon);
    pokemonRefetch();
  };

  return { handleSelectChain, evolutionChainData, handleSelectEvolution, pokemonData};
};

export default useEvolution;
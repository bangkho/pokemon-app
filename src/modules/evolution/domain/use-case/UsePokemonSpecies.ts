"use client"

import { useState, useEffect } from 'react';
import { getItem, setItem } from '@/utils/storage/LocalStorage';
import PokemonSpecies from '../../data/PokemonSpecies';
import useEvolution from './UseEvolution';

const usePokemonSpecies = () => {
  const [selectedPokemon, setSelectedPokemon] = useState<string>('');
  const { data, refetch } = PokemonSpecies(selectedPokemon, !!selectedPokemon);
  const { handleSelectChain, handleSelectEvolution, evolutionChainData, pokemonData } = useEvolution();

  useEffect(() => {
    if (data) {
      handleSelectChain(data?.evolution_chain?.url?.split('/')[6]);
    }
    if(evolutionChainData){
      if((evolutionChainData?.chain?.evolves_to.length !== 0) && (evolutionChainData?.chain?.species?.name === selectedPokemon)){
        handleSelectEvolution(evolutionChainData?.chain?.evolves_to[0]?.species?.name)
      } else if((evolutionChainData?.chain?.evolves_to[0]?.evolves_to.length !== 0) && (evolutionChainData?.chain?.evolves_to[0]?.species?.name === selectedPokemon)){
        handleSelectEvolution(evolutionChainData?.chain?.evolves_to[0]?.evolves_to[0]?.species?.name)
      }
    }
  }, [data, selectedPokemon, handleSelectChain, evolutionChainData, handleSelectEvolution]);

  const handleSelectPokemon = (pokemon: string) => {
    setSelectedPokemon(pokemon);
    refetch();
  };

  return { handleSelectPokemon, pokemonSpeciesData: data, evolutionChainData, pokemonEvolveTo: pokemonData, handleSelectEvolution};
};

export default usePokemonSpecies;
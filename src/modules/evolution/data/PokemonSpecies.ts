"use client"

import type PokemonSpecies from "@/modules/evolution/domain/model/PokemonSpecies";

import { usePokemonQuery } from "@/utils/API/query"

function PokemonSpecies(id: string, enabled: boolean) {
  const {data , isLoading, refetch } = usePokemonQuery('pokemonSpecies', `pokemon-species/${id}`, { staleTime: Infinity, enabled })
  return { data: data as PokemonSpecies, isLoading, refetch}
}

export default PokemonSpecies

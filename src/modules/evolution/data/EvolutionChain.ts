"use client"

import { usePokemonQuery } from "@/utils/API/query"

import type EvolutionChain from "@/modules/evolution/domain/model/EvolutionChain";

function EvolutionChain(id: string, enabled: boolean) {
  const {data , isLoading, refetch } = usePokemonQuery('evolutionChain', `evolution-chain/${id}`, { staleTime: Infinity ,enabled })
  return { data: data as EvolutionChain, isLoading, refetch}
}

export default EvolutionChain
